# H2O API productionisation demo

This repo demonstrates creating a Java API from an exported H2O model.

## Build

Build the application with Maven:

```sh
mvn package
```

This will generate a '.jar' file in the 'target' directory.

## Run

First, train and export a model with:

```sh
./train_model.py
```

This will generate an exported model artifact called something like
'GBM_model_python_1558528064680_591.zip'.

Then serve the model with the built Java application:

```sh
java -jar target/h2o-api-0.1.0-jar-with-dependencies.jar <model artifact>
```

You can then send the API requests on the default port (4567) from another
terminal:

```sh
curl -X POST localhost:4567/predict -d '{"age": 71, "race": 1, "psa": 20.2, "gleason": 8}'
```

## Dockerise

An example Dockerfile packaging the model and API is included. To build the
docker image:

```sh
docker build . -t h2o-api
```

And to run it:

```sh
docker run --rm -p 4567:4567 h2o-api
```

Requests can be sent to the API running in the docker container in the same way
as above.
