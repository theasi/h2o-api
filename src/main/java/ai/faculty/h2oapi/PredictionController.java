package ai.faculty.h2oapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static spark.Spark.*;
import com.google.gson.Gson;

import hex.genmodel.easy.exception.PredictException;

class InvalidRequest extends Exception {}

class Inputs {
  public final String age;
  public final String race;
  public final String psa;
  public final String gleason;

  public Inputs(String age, String race, String psa, String gleason) {
      this.age = age;
      this.race = race;
      this.psa = psa;
      this.gleason = gleason;
  }

  public static Inputs fromJson(String json) throws InvalidRequest {
    Gson gson = new Gson();
    Inputs inputs;
    try {
      inputs = gson.fromJson(json, Inputs.class);
    } catch (Exception e) {
      throw new InvalidRequest();
    }
    // Check that all fields have actually been read
    if (inputs.age == null || inputs.race == null || inputs.psa == null || inputs.gleason == null) {
      throw new InvalidRequest();
    }
    return inputs;
  }

  public String toString() {
    return String.format(
      "Inputs(age=%s, race=%s, psa=%s, gleason=%s)",
      age, race, psa, gleason
    );
  }
}

class Output {
  private final int capsule;

  Output(int capsule) {
    this.capsule = capsule;
  }

  public String toJson() {
    Gson gson = new Gson();
    return gson.toJson(this);
  }
}

public class PredictionController {

  final Logger logger = LoggerFactory.getLogger(PredictionController.class);

  public PredictionController(PredictionService service) {

    post("/predict", (request, response) -> {
      String body = request.body();
      try {
        Inputs inputs = Inputs.fromJson(body);
        logger.info("Received request " + inputs.toString());
	int capsule = service.predict(
          inputs.age, inputs.race, inputs.psa, inputs.gleason
        );
        return new Output(capsule).toJson();
      } catch (InvalidRequest e) {
        logger.info("Received invalid request '" + body + "'");
        response.status(400);
        return "{\"error\": \"Bad request\"}";
      } catch (PredictException e) {
        logger.error("Error occurred making prediction", e);
        response.status(500);
        return "{\"error\": \"Error making prediction\"}";
      }
    });

    after((request, response) -> {
      response.type("application/json");
    });

  }
}
