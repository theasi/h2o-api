package ai.faculty.h2oapi;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

  final static Logger logger = LoggerFactory.getLogger(App.class);

  public static void main(String[] args) throws IOException {

    // Load model path from CLI
    final String modelPath;
    try {
      modelPath = args[0];
    } catch (ArrayIndexOutOfBoundsException e) {
      throw new RuntimeException("Must provide path of model to serve");
    }

    // Start server
    logger.info("Serving model from %s", modelPath);
    final PredictionService service = new PredictionService(modelPath);
    new PredictionController(service);
  }
}
