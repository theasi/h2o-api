package ai.faculty.h2oapi;

import java.io.IOException;

import hex.genmodel.easy.RowData;
import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.prediction.*;
import hex.genmodel.easy.exception.PredictException;
import hex.genmodel.MojoModel;

public class PredictionService {

  private final EasyPredictModelWrapper model;

  PredictionService(String modelPath) throws IOException {
    this.model = new EasyPredictModelWrapper(MojoModel.load(modelPath));
  }

  public int predict(String age, String race, String psa, String gleason) throws PredictException {

    RowData row = new RowData();
    row.put("AGE", age);
    row.put("RACE", race);
    row.put("PSA", psa);
    row.put("GLEASON", gleason);

    BinomialModelPrediction prediction = model.predictBinomial(row);
    return prediction.labelIndex;
  }

}
