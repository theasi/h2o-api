FROM openjdk:8

RUN mkdir /usr/src/app
COPY target/h2o-api-0.1.0-jar-with-dependencies.jar /usr/src/app/h2o-api.jar
COPY GBM_model_python_1558528064680_591.zip /usr/src/app/model.zip

WORKDIR /usr/src/app
CMD ["java", "-jar", "h2o-api.jar", "model.zip"]
