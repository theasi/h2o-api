#!/usr/bin/env python

import h2o
from h2o.estimators.gbm import H2OGradientBoostingEstimator

h2o.init()

data = h2o.load_dataset("prostate.csv")
data["CAPSULE"] = data["CAPSULE"].asfactor()

model = H2OGradientBoostingEstimator(
    distribution="bernoulli", ntrees=100, max_depth=4, learn_rate=0.1
)

model.train(
    y="CAPSULE", x=["AGE", "RACE", "PSA", "GLEASON"], training_frame=data
)

model.download_mojo(path="./")
